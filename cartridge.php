<?php

include 'settings.php';

?>

<!doctype html>
<html lang="ru">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>Hello, world!</title>
  </head>
  <body>
    
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <center>
            <h1>Картридж 1</h1>
          </center>
        </div>
        <div class="col-md-4"><img class="img-fluid scimg" src="<?php echo $siCart; ?>" alt=""></div>
        <div class="col-md-8">
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat, architecto eveniet aperiam reprehenderit cupiditate dolorum? Sint molestias eius, praesentium eveniet, autem laudantium ad, vel et quod, eum nesciunt pariatur debitis.
          Repudiandae, eaque, tempora. Porro aspernatur quae accusantium ullam, sapiente quas molestias facilis, consequuntur, cupiditate maxime hic illo nesciunt ut. Praesentium iusto modi quod amet esse sit reprehenderit ab, mollitia molestias.
          Iusto, porro. Voluptatum doloremque nemo, aperiam eligendi eaque voluptatibus consequuntur temporibus mollitia accusamus rerum porro ipsum, debitis ratione amet aut, ad ab quis id quas. Labore aut voluptatum aliquam fugiat.
          Maxime deserunt laborum sed itaque corrupti reiciendis eius suscipit minima numquam non illo perferendis nam ipsam, sapiente quod ullam amet ex in, quam dolorum officia tempore qui iusto. Obcaecati, mollitia.
          Perspiciatis nobis amet, maiores, perferendis, ipsam dolorum iure atque enim sequi qui quibusdam obcaecati optio doloribus dolores, recusandae inventore nesciunt hic debitis corrupti! Praesentium fugit quidem, cupiditate earum nulla accusamus?
          Necessitatibus, ipsa in aut rerum, modi, cumque, dolorem laudantium expedita omnis accusamus animi dicta quisquam illum iusto dolore natus est molestiae repellendus asperiores nisi facere? Enim voluptatum aspernatur, nulla sint.
          Quod optio, autem iusto, enim consequatur eaque dolor itaque minima perferendis. Excepturi veritatis eum quaerat deserunt officia obcaecati eos voluptatibus cupiditate deleniti perspiciatis placeat dolorem ipsum, eligendi quas nam molestias?
          Architecto quidem, corporis esse ratione, at ut debitis! Totam soluta necessitatibus consequatur reprehenderit esse cumque magnam, expedita exercitationem quos illo quaerat obcaecati tempora excepturi corporis minima! Ea reiciendis nulla, tempore!
          Iste cupiditate nisi rem illum fuga corrupti, facilis officia sapiente exercitationem id quaerat quod, sint voluptate quo? Sit accusantium eius tempora rerum porro, iure nemo adipisci accusamus quod eveniet sunt.
          Quidem mollitia, quisquam reprehenderit, distinctio quasi molestiae commodi qui soluta, voluptatem minima ullam iure a enim placeat rerum, quod. In laborum quisquam odit at magni possimus praesentium harum ut facere!</p>
        </div>
        <div class="col-md-12"><hr></div>
        <div class="col-4"><input type="button" value="Кнопка 1" class="btn btn-primary"></div>
        <div class="col-4"><center><input type="button" value="Кнопка 2" class="btn btn-primary"></center></div>
        <div class="col-4"><input style="float: right;" type="button" value="Кнопка 3" class="btn btn-primary"></div>
        <div class="col-12"><br></div>
      </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

  </body>
</html>
