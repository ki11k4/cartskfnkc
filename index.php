<?php

include 'settings.php';

?>

<!doctype html>
<html lang="ru">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>Hello, world!</title>
  </head>
  <body>
    
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <center>
            <h1>Меню</h1>
          </center>
        </div>
        <div class="col-md-3"><a href="cartridge.php"><img src="<?php echo $siCart; ?>" alt="Картриджи" class="img-fluid img-thumbnail rounded shadow mmimg"><br><center>Картриджи</center></a></div>
        <div class="col-md-3"><a href="printer.php"><img src="<?php echo $siPrint; ?>" alt="" class="img-fluid img-thumbnail rounded shadow mmimg"><br><center>Принтер</center></a></div>
        <div class="col-md-3" id="zap"><a href="#zap"><img src="<?php echo $siToner; ?>" alt="" class="img-fluid img-thumbnail rounded shadow mmimg"><br><center>Заправка</center></a></div>
        <div class="col-md-3" id="rep"><a href="#rep"><img src="<?php echo $siReport; ?>" alt="" class="img-fluid img-thumbnail rounded shadow mmimg"><br><center>Отчёты</center></a></div>
        <div class="col-md-3" id="adm"><a href="#adm"><img src="<?php echo $siAdmin; ?>" alt="" class="img-fluid img-thumbnail rounded shadow mmimg"><br><center>Админка</center></a></div>
      </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

  </body>
</html>
