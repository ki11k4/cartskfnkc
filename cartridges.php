<?php

include 'settings.php';

?>

<!doctype html>
<html lang="ru">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>Hello, world!</title>
  </head>
  <body>
   
   <div class="container-fluid">
      <div class="row">

        <div class="col-md-12"><br></div>
        <div class="col-4"><input type="button" value="Кнопка 1" class="btn btn-primary"></div>
        <div class="col-4"><center><input type="button" value="Кнопка 2" class="btn btn-primary"></center></div>
        <div class="col-4"><input style="float: right;" type="button" value="Кнопка 3" class="btn btn-primary"></div>
        <div class="col-md-12"><hr></div>

        <div class="col-md-12">
          <table class="table">
            <tr>
              <th scope="col">id</th>
              <th scope="col">Модель</th>
              <th scope="col">Цвет</th>
              <th scope="col">Примечание</th>
              <th scope="col"></th>
            </tr>

            <?php 
              if ( $_GET['is_color'] == 1 ) {
                $selPrint = mysqli_query($connection, $select_color_cartridge);
              } else {
                $selCart = mysqli_query($connection, $select_all_mcartridges);
              }
              while ( $cartridge = mysqli_fetch_assoc($selCart) ) { ?>
            <tr>
              <th scope="row"><?php echo $cartridge['id']; ?></th>
              <td><?php echo $cartridge['model']; ?></td>
              <td><?php echo $cartridge['color']; ?></td>
              <td><?php echo $cartridge['description']; ?></td>
              <td><a href="cartridge.php?cartridge_id=<?php echo $cartridge['id']?>"><input type="button" class="btn btn-primary" value="Посмотреть"></a></td>
            </tr>
            <?php } ?>

          </table>
        </div>
      </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

  </body>
</html>
