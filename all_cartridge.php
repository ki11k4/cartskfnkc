<?php

include 'settings.php';

?>

<!doctype html>
<html lang="ru">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>Hello, world!</title>
  </head>
  <body>
    
    <div class="container-fluid">
      <div class="row">

        <div class="col-4"><input type="button" value="Кнопка 1" class="btn btn-primary"></div>
        <div class="col-4"><center><input type="button" value="Кнопка 2" class="btn btn-primary"></center></div>
        <div class="col-4"><input style="float: right;" type="button" value="Кнопка 3" class="btn btn-primary"></div>
        <div class="col-md-12"><hr></div>

        <div class="col-md-12">
          <table class="table">
            <tr>
              <th scope="col">id</th>
              <th scope="col">Модель</th>
              <th scope="col">Статус</th>
              <th scope="col">Филиал (принадлежность)</th>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>725</td>
              <td>Склад Юность</td>
              <td>МЦ "Юность"</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>505</td>
              <td>Заправка</td>
              <td>КБ №101</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td>728</td>
              <td>У сотрудника <a href="#">...</a></td>
              <td>МЦ "Клязьма"</td>
            </tr>
          </table>
        </div>
      </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

  </body>
</html>
