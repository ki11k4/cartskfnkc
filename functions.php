<?php

include 'settings.php';


/**
 * Функция удаления значения из массива полученного из функции findCompabilityCartridge()
 */
function delOneElement($arrName, $find) {
    foreach($arrName as $key => $item){
        if ($item == $find){
          unset($arrName[$key]);
        }
    }
    return $arrName;
}

/**
 * Функция поиска совместимых картриджей
 * idc => id картриджа для которого ищем
 * Если находим совместимые картриджи, то возвращаем массив с их id
 * Иначе возвращаем 0 (ноль)
 * 
 * $arrResult - Массив для сбора итогового результата
 * 
 * $query_org - Пытаемся найти по оригинальному картриджу
 * $query_cmp - Пытаемся найти по совместимому картриджу, если по оригинальному не получилось
 */
function findCompabilityCartridge($idc, $connection) {
    $arrResult = [];

    $query_org = "SELECT * FROM `compatibility_cartridges` WHERE id_model_cartridge = " . $idc;
    $query_cmp = "SELECT * FROM `compatibility_cartridges` WHERE id_model_comp = " . $idc;

    $sel_org_cart = mysqli_query($connection, $query_org);
    if (mysqli_num_rows($sel_org_cart) > 0) {
        while ($data = mysqli_fetch_assoc($sel_org_cart)) {
            array_push($arrResult, $data['id_model_comp']);
        }
        return $arrResult;
    } else {
        $sel_cmp_cart = mysqli_query($connection, $query_cmp);
        if (mysqli_num_rows($sel_cmp_cart) > 0) {
            while ($data = mysqli_fetch_assoc($sel_cmp_cart)) {
                array_push($arrResult, $data['id_model_cartridge']);
                $sel_org_cart_add = mysqli_query($connection, "SELECT * FROM `compatibility_cartridges` WHERE id_model_cartridge = " . $data['id_model_cartridge']);
                while ($dop_data = mysqli_fetch_assoc($sel_org_cart_add)) {
                    array_push($arrResult, $dop_data['id_model_comp']);
                }                
            }
            $arrResult = delOneElement($arrResult, $idc);
            return($arrResult);
        } else {
            return 0;
        }
    }
}


function findCompabilityCartridgeByName($idc, $connection) {
    $arrResult = [];

    $queryNameCartridge = "SELECT * FROM `model_cartridge` WHERE id = ";

    $resultId = findCompabilityCartridge($idc, $connection);
    if ($resultId == 0 ) {
        return 0;
    } else {
        foreach ($resultId as $key => $value) {
            $resultName = mysqli_query($connection, $queryNameCartridge . $value);
            while ($data = mysqli_fetch_assoc($resultName)) {
                $arrResult[$value] = $data['model'];
            }
        }
    
        return $arrResult;
    }

}

?>