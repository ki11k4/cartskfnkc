<?php

$imgFolder = "./img/";						# Папка с изображениями

/* STATIC IMAGE */
$siCart    = $imgFolder . "cartridge.jpg";	# Изображение картриджа
$siPrint   = $imgFolder . "printer.jpg";	# Изображение принтера
$siToner   = $imgFolder . "toner.jpg";		# Изображение тонера
$siReport  = $imgFolder . "otchet.jpg";		# Изображение отчётов
$siAdmin   = $imgFolder . "admin.png";		# Изображение админки

?>